"use strict";

const main = function () {
    const difficulties = [
        {rows: 14, cols: 18, mines: 40},
        {rows: 20, cols: 23, mines: 99}
    ]
    const colormap = {
        '1': 'blue',
        '2': 'green',
        '3': 'red',
        '4': 'DodgerBlue',
        '5': 'brown',
        '6': 'cyan',
        '7': 'black',
        '8': 'grey',
    }

    const default_difficulty = 0;
    const game = new MSGame();
    const grid = $('#grid');
    const facePic = $('#face-pic');
    const statusText = $('#status');
    const numMines = $('#num-mines');
    const time = $('#current-time');
    let started = false;
    let timeInterval;

    let gridInfo = difficulties[default_difficulty];

    function createGridRender(gridInfo) {
        grid.css('grid-template-columns', `repeat(${gridInfo.cols}, 1fr)`);
        grid.empty();
        for (let i = 0; i < gridInfo.rows; i++) {
            for (let j = 0; j < gridInfo.cols; j++) {
                const cell = $('<div>').addClass('col')
                    .addClass('hidden')
                    .attr('data-row', i)
                    .attr('data-col', j);

                grid.append(cell);
            }
        }
    }

    function updateGridRender(lost = false) {
        const divs = grid.find('div');
        const rendering = game.getRendering();
        console.log(rendering);
        for (let i = 0; i < gridInfo.rows; i++) {
            for (let j = 0; j < gridInfo.cols; j++) {
                const cell = divs[i * gridInfo.cols + j];
                const cellState = rendering[i][j];
                if (cellState !== 'H' && cellState !== 'F' && cellState !== 'M') {
                    cell.classList.remove('hidden');
                    cell.classList.remove('flag');
                    if (cellState !== '0') {
                        // cell.innerText = cellState;
                        cell.innerHTML = "<svg class='number' viewBox=\"0 0 24 24\">\n" +
                            `  <text x=\"50%\" y=\"50%\" text-anchor='middle' fill='${colormap[cellState]}' alignment-baseline='central'>${cellState}</text>\n` +
                            "</svg>"
                    }
                } else if (lost) {
                    if (cellState === 'M') {
                        if (!cell.classList.contains('flag')) {
                            cell.classList.remove('hidden');
                            cell.classList.add('mine');
                        }
                    } else if (cellState === 'F') {
                        const span = document.createElement('span');
                        span.innerText = '✕';
                        cell.appendChild(span);
                    }
                }
            }
        }
    }

    function flagCell(cell) {
        if (game.mark(cell.data('row'), cell.data('col'))) {
            if (cell.hasClass('flag')) {
                cell.removeClass('flag');
                numMines.text(parseInt(numMines.text()) + 1);
            } else {
                numMines.text(parseInt(numMines.text()) - 1);
                cell.addClass('flag');
            }
        } else {
            alert('Invalid flag position');
        }
    }

    function handleCellClick(event) {
        if (event.which === 3) { // Right mouse button
            const cell = $(this);
            flagCell(cell);
        } else if (event.which === 1) {
            if (!started) {
                started = true;
                timeInterval = setInterval(function () {
                    time.text(parseInt(time.text()) + 1);
                }, 1000);
            }
            const cell = $(this);
            const rowIdx = cell.data('row');
            const colIdx = cell.data('col');
            game.uncover(rowIdx, colIdx);
            const status = game.getStatus();
            if (status.done) {
                if (status.exploded) {
                    gameLose();
                } else {
                    gameWin();
                }
            } else {
                facePic.attr('src', 'assets/common/res/iconmonstr-smiley-normal.svg');
                updateGridRender(false);
            }
        }
    }

    function gameLose() {
        // Sort of a hack to wait for setting normal face listener to fire
        setTimeout(() => {
            facePic.attr('src', 'assets/common/res/iconmonstr-smiley-dead.svg');
        }, 100);

        statusText.text('YOU LOSE!');
        statusText.css('visibility', 'visible');
        removeGridListeners();
        updateGridRender(true);
        clearInterval(timeInterval);
    }

    function gameWin() {
        facePic.attr('src', 'assets/common/res/iconmonstr-smiley-won.svg');
        statusText.text('YOU WIN!');
        statusText.css('visibility', 'visible');
        removeGridListeners();
        updateGridRender(false);
        clearInterval(timeInterval);
    }

    function setWorriedFace(event) {
        if (event.which === 1) {
            facePic.attr('src', 'assets/common/res/iconmonstr-smiley-worried.svg')
        }
    }

    function setNormalFace(event) {
        if (event.which === 1) {
            facePic.attr('src', 'assets/common/res/iconmonstr-smiley-normal.svg')
        }
    }

    function onTapHold(event) {
        flagCell($(event.target));
    }

    function addGridListeners() {
        grid.on('mouseup', setNormalFace);
        grid.on('mouseup', '.col.hidden', handleCellClick);
        grid.on('taphold', onTapHold);
        grid.on('mousedown', setWorriedFace);
    }

    function removeGridListeners() {
        grid.off('mouseup', '.col.hidden', handleCellClick);
        grid.off('taphold', onTapHold);
        grid.off('mouseup', setNormalFace);
        grid.off('mousedown', setWorriedFace);
    }

    /**
     * Initializes a new game with specified parameters
     * Handles initializing game logic and rendering
     */
    function gridInit() {
        removeGridListeners();
        facePic.attr('src', 'assets/common/res/iconmonstr-smiley-normal.svg');
        game.init(gridInfo.rows, gridInfo.cols, gridInfo.mines);
        createGridRender(gridInfo);

        if (timeInterval) {
            clearInterval(timeInterval);
        }
        started = false;
        statusText.css('visibility', 'hidden');
        addGridListeners();
        time.text('0');

        numMines.text(gridInfo.mines.toString());
    }

    function repositionStatusBar() {
        const statusDiv = document.getElementById('status-div');
        statusDiv.style.width = (parseFloat(window.getComputedStyle(grid.get(0)).getPropertyValue('width').replace('/px/', '')) - 5).toString() + 'px';
    }

    gridInit();

    facePic.on('tap', function () {
        gridInit(difficulties[default_difficulty]);
    });

    $('.menuButton').on('mousedown', function (event) {
        const button = event.target;
        if (button.id === 'easy') {
            gridInfo = difficulties[0];
            gridInit();
        } else if (button.id === 'hard') {
            gridInfo = difficulties[1];
            gridInit();
        }
    });

    $(window).resize(function () {
        repositionStatusBar();
    });

    $(window).on("orientationchange", function () {
        repositionStatusBar();
    });

    $(document).ready(function () {
        repositionStatusBar();
        grid.on('contextmenu', () => false);
    });

    $.mobile.loading().hide();
}

main();
